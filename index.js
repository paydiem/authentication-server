const express = require('express');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
const cors = require('cors'); //Resolves the CORS issue.

const app = express();
const PORT = process.env.PORT || 8080;
const SECRET_KEY = '#dadliCAN';
const JWT_OPTIONS = {
    expiresIn: '5m'
}

const users = [
    { id : 1, userName : 'ali', password : 'bhagat' },
    { id : 2, userName : 'ernest', password : 'aaron' }
];

app.use(bodyParser.json());
app.use(cors());

app.post('/login', (req, res) => {
    const { userName, password } = req.body;

    if (!userName || !password) {
        res
        .status(400)
        .send(`You must provide a username and password to view your paydiem account.`);
        return;
    }

    const user = users.find(user => user.userName === userName && user.password === password);

    if (!user) {
        res
        .status(401)
        .send(`We can't seem to find a match for your username or password`);
        return;
    }

    const token = jwt.sign({
        sub: user.id,
        username: user.userName
    }, SECRET_KEY, JWT_OPTIONS);

    res
    .status(200)
    .send({token});
});

app.get('/status', (req, res) => {
    const localTime = (new Date()).toLocaleDateString();

    res
    .status(200)
    .send(`Local time is ${localTime}.`);
});

app.get('*', (req, res) => {
    res.sendStatus(404);
});

app.listen(PORT, () => {
    console.log(`Successfully running app on port ${PORT}.`);
});